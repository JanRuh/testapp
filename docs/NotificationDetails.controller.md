[Back](../../dev)

<a name="sbb.repairAndClean.controller.module_NotificationDetails"></a>

## NotificationDetails ⇐ <code>sbb.repairAndClean.controller.BaseController</code>
Controller for the Detail View

**Extends**: <code>sbb.repairAndClean.controller.BaseController</code>  
**Author**: Harald Hammer  

* [NotificationDetails](#sbb.repairAndClean.controller.module_NotificationDetails) ⇐ <code>sbb.repairAndClean.controller.BaseController</code>
    * [~onInit()](#sbb.repairAndClean.controller.module_NotificationDetails..onInit)
    * [~onNavBack()](#sbb.repairAndClean.controller.module_NotificationDetails..onNavBack)

<a name="sbb.repairAndClean.controller.module_NotificationDetails..onInit"></a>

### NotificationDetails~onInit()
Called when a controller is instantiated and its View controls (if available) are already created.Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.

**Kind**: inner method of [<code>NotificationDetails</code>](#sbb.repairAndClean.controller.module_NotificationDetails)  
**Access**: public  
<a name="sbb.repairAndClean.controller.module_NotificationDetails..onNavBack"></a>

### NotificationDetails~onNavBack()
Event handler when the NavBackButton is pressed

**Kind**: inner method of [<code>NotificationDetails</code>](#sbb.repairAndClean.controller.module_NotificationDetails)  
**Access**: public  

* * *

Copyright: 2020 - "End of Time" Jan Ruh

[![view on npm](https://digital.sbb.ch/media/pages/brand_elemente/logo/769830240-1579638676/logo.svg)](../../dev)