[Back](../../dev)

<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport"></a>

## RepairAndCleanReport ⇐ <code>sbb.repairAndClean.controller.BaseController</code>
Controller for the Main View

**Extends**: <code>sbb.repairAndClean.controller.BaseController</code>  
**Version**: 1.0.1  
**Author**: Heinrich Hammel  

* [RepairAndCleanReport](#sbb.repairAndClean.controller.module_RepairAndCleanReport) ⇐ <code>sbb.repairAndClean.controller.BaseController</code>
    * [~onInit()](#sbb.repairAndClean.controller.module_RepairAndCleanReport..onInit)
    * [~_bindUserListTable()](#sbb.repairAndClean.controller.module_RepairAndCleanReport.._bindUserListTable)
    * [~_tableFactoryFunction(sId, oContext)](#sbb.repairAndClean.controller.module_RepairAndCleanReport.._tableFactoryFunction) ⇒ <code>sap.m.ColumnListItem</code>
    * [~onItemPress(oEvent)](#sbb.repairAndClean.controller.module_RepairAndCleanReport..onItemPress)
    * [~_onObjectMatched(oPatternMatchedEvent)](#sbb.repairAndClean.controller.module_RepairAndCleanReport.._onObjectMatched)
    * [~onSelectNotification(oEvent)](#sbb.repairAndClean.controller.module_RepairAndCleanReport..onSelectNotification)
    * [~onHandleSearch(oEvent)](#sbb.repairAndClean.controller.module_RepairAndCleanReport..onHandleSearch)

<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport..onInit"></a>

### RepairAndCleanReport~onInit()
Event handler when the view is initialized
Triggers once

**Kind**: inner method of [<code>RepairAndCleanReport</code>](#sbb.repairAndClean.controller.module_RepairAndCleanReport)  
**Access**: protected  
<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport.._bindUserListTable"></a>

### RepairAndCleanReport~\_bindUserListTable()
Add two values.

**Kind**: inner method of [<code>RepairAndCleanReport</code>](#sbb.repairAndClean.controller.module_RepairAndCleanReport)  
<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport.._tableFactoryFunction"></a>

### RepairAndCleanReport~\_tableFactoryFunction(sId, oContext) ⇒ <code>sap.m.ColumnListItem</code>
Creates a table template for the notification list

**Kind**: inner method of [<code>RepairAndCleanReport</code>](#sbb.repairAndClean.controller.module_RepairAndCleanReport)  
**Returns**: <code>sap.m.ColumnListItem</code> - oColumnListItem - Row of the Table  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| sId | <code>string</code> | the created control ID |
| oContext | <code>sap.ui.model.Context</code> | the context object which allows accessing data from the list entry |

<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport..onItemPress"></a>

### RepairAndCleanReport~onItemPress(oEvent)
Event handler when a notificationlistitem is pressed
Updates the bottom part of the view with detail information

**Kind**: inner method of [<code>RepairAndCleanReport</code>](#sbb.repairAndClean.controller.module_RepairAndCleanReport)  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| oEvent | <code>sap.ui.base.Event</code> | the table selectionChange event |

**Example**  
```js
The second row gets pressed, then the bounded Object will be saved.
Later if the object is needed, you can find it there:
this._oView.getModel("LocalNotificationListModel").setProperty("/selection/rowSelected");
```
<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport.._onObjectMatched"></a>

### RepairAndCleanReport~\_onObjectMatched(oPatternMatchedEvent)
handler on route match

**Kind**: inner method of [<code>RepairAndCleanReport</code>](#sbb.repairAndClean.controller.module_RepairAndCleanReport)  
**Access**: public  

| Param | Type | Description |
| --- | --- | --- |
| oPatternMatchedEvent | <code>sap.ui.base.Event</code> | the pattern match event in route 'object' |

<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport..onSelectNotification"></a>

### RepairAndCleanReport~onSelectNotification(oEvent)
Event handler when a row of the notification table is pressed.

**Kind**: inner method of [<code>RepairAndCleanReport</code>](#sbb.repairAndClean.controller.module_RepairAndCleanReport)  
**Access**: public  

| Param | Type |
| --- | --- |
| oEvent | <code>sap.ui.base.Event</code> | 

<a name="sbb.repairAndClean.controller.module_RepairAndCleanReport..onHandleSearch"></a>

### RepairAndCleanReport~onHandleSearch(oEvent)
Event handler when something is typed in the search field.

**Kind**: inner method of [<code>RepairAndCleanReport</code>](#sbb.repairAndClean.controller.module_RepairAndCleanReport)  
**Access**: public  

| Param | Type |
| --- | --- |
| oEvent | <code>sap.ui.base.Event</code> | 

**Example** *(Example usage of onHandleSearch.)*  
```js
onHandleSearch("s");
returns all locations which contains the letter s
```

* * *

Copyright: 2020 - "End of Time" Jan Ruh

[![view on npm](https://digital.sbb.ch/media/pages/brand_elemente/logo/769830240-1579638676/logo.svg)](../../dev)