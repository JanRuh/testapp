[Back](../../dev)

<a name="sbb.repairAndClean.model.module_models"></a>

## models ⇒ <code>sap.ui.model.json.JSONModel</code>
Create Device JSONModel

**Returns**: <code>sap.ui.model.json.JSONModel</code> - oModel - Device JSONModel  
**Access**: public  

* * *

Copyright: 2020 - "End of Time" Jan Ruh

[![view on npm](https://digital.sbb.ch/media/pages/brand_elemente/logo/769830240-1579638676/logo.svg)](../../dev)