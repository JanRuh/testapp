sap.ui.define([
	"sbb/repairAndClean/controller/BaseController"
], function (Controller) {
	"use strict";

	/**
	 * Controller for the Detail View
	 * @extends sbb.repairAndClean.controller.BaseController
	 * @author Harald Hammer
	 * @module  sbb.repairAndClean.controller.RepairAndCleanReport
	 * @name sbb.repairAndClean.controller.NotificationDetails
	 */

	return Controller.extend("sbb.repairAndClean.controller.NotificationDetails", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @public
		 */
		onInit: function () {


		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Event handler when the NavBackButton is pressed
		 * @public		 
		 */
		onNavBack: function () {
			this.getOwnerComponent().getRouter().navTo("RouteRepairAndCleanReport", true);
		}

	});

});