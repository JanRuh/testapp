sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (BaseController) {
	"use strict";

	/**
	 * Base Controller of repairAndClean
	 * @class 
	 * @name sbb.repairAndClean.controller.BaseController
	 * @extends sap.ui.core.Controller
	 */

	return BaseController.extend("sbb.repairAndClean.controller.BaseController", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Event handler when the view is initialized
		 * Triggers once
		 * @memberOf sbb.repairAndClean.controller.BaseController
		 * @public
		 * 
		 * @todo Write the documentation.
		 * @todo Implement this function.
		 */
		onInit: function () {

		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		/**
		 * Convenience method for getting the text of the language component by ID.
		 * @memberOf sbb.repairAndClean.controller.BaseController
		 * @public
		 * @param {string} sTextID the text ID
		 * @param {array} aArgs - list of string values for replacing the placeholders in the text
		 * @returns {string} the text for the specified ID
		 */
		_getText: function (sTextID, aArgs) {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(sTextID, aArgs);
		},

		/**
		 * Callback for adding two numbers.
		 *
		 * @memberOf sbb.repairAndClean.controller.BaseController
		 * @callback addStuffCallback
		 * @param {int} sum - An integer.
		 */

		/**
		 * Add two numbers together, then pass the results to a callback function.
		 *
		 * @memberOf sbb.repairAndClean.controller.BaseController
		 * @param {int} x - An integer.
		 * @param {int} y - An integer.
		 * @param {addStuffCallback} callback - A callback to run.
		 */
		addStuff: function (x, y, callback) {
			callback(x + y);
		},

		/**
		 * Binds the given control to a specific model entity.
		 * @public
		 * @memberOf sbb.repairAndClean.controller.BaseController
		 * @param {sap.ui.core.Control} oControl - the control which needs to be bound
		 * @param {object} oParams.parameters - some parameters like sorter, filter, template, aggregation etc. 
		 */
		_bindAggregationsToControl: function (oControl, oParams) {
			var aSplitPath = oParams.path.split(">");
			var oModel;

			if (aSplitPath.length > 1) {
				oModel = oControl.getModel(aSplitPath[0]);
			} else {
				oModel = oControl.getModel();
			}

			if (oModel && oModel.iSizeLimit < 2000) {
				//the standard size limit needs to be increased (until growing functionality is implemented)
				oModel.setSizeLimit(2000);
			}

			oControl.bindAggregation(oParams.aggregation, {
				path: oParams.path,
				filters: oParams.aFilters,
				sorter: oParams.aSorters,
				template: oParams.oTemplate,
				factory: oParams.fnFactory,
				groupHeaderFactory: oParams.fnGroupHeaderFactory,
				parameters: oParams.parameters
			});
		}

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

	});

});