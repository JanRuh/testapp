sap.ui.define([
	"sbb/repairAndClean/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/ColumnListItem",
	"sap/m/Text"
], function (Controller, JSONModel, ColumnListItem, Text) {
	"use strict";

	/**
	 * Controller for the Main View
	 * @extends sbb.repairAndClean.controller.BaseController
	 * @module  sbb.repairAndClean.controller.RepairAndCleanReport
	 * @Version 1.0.1
	 * @author Heinrich Hammel
	 * @name sbb.repairAndClean.controller.RepairAndCleanReport
	 * @exports sbb.repairAndClean.controller.RepairAndCleanReport
	 */

	return Controller.extend("sbb.repairAndClean.controller.RepairAndCleanReport", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Event handler when the view is initialized
		 * Triggers once
		 * @method onInit
		 * @protected
		 */

		onInit: function () {

			this._oViewModel = new JSONModel({
				tableNoDataText: this._getText("noDataText"),
				tableBusyDelay: 0,
				FileSelected: false,
				results: [],
				selection: {
					rowSelected: false,
					row: {}
				}
			});
			this._oView = this.getView();
			this._oView.setModel(this._oViewModel, "LocalNotificationListModel");

			//	var model = new sap.ui.model.json.JSONModel();
			//	model.setData(oData);
			//	this.getView().byId("idProductsTable").setModel(model);

			this.getOwnerComponent().getRouter().getRoute("RouteRepairAndCleanReport").attachPatternMatched(this._onObjectMatched, this);

		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

		/**
		 * Add two values.
		 */
		_bindUserListTable: function (aData) {
			var sPath = "/results";
			var LocalNotificationListModel = this._oView.getModel("LocalNotificationListModel");
			LocalNotificationListModel.setProperty(sPath, aData);
			this._oTable.setModel(LocalNotificationListModel);
			var oParameters = {
				aggregation: "items",
				path: sPath,
				fnFactory: this._tableFactoryFunction.bind(this)
			};
			this._bindAggregationsToControl(this._oTable, oParameters);
		},

		/**
		 * Reads the NotificationSet data, only smart orders
		 * @method _fnReadNotifiactionDataSet
		 * @private
		 */
		_fnReadNotifiactionDataSet: function () {
			//	this.getModelByView("LocalNotificationListModel").setProperty("/isBusyRetrievingOrders", true);
			this._oTable.setBusy(true);
			var oData = {
				results: [{
					"Status": "offen",
					"Priority": "hoch",
					"FzNr": "101 1212 12 -12",
					"Type": "BT1 500",
					"Object": "Steckdose Seitenwand im Wagen",
					"State": "kein Strom",
					"Team": "L",
					"Date": "21.05.2019 11.15",
					"longtext": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
					"system": "Bremsen pneum. & Mg-Bremse -> Klotzbremsanlage -> Bremssohle",
					"symptom": "abgenutzt"
				}, {
					"Status": "offen",
					"Priority": "hoch",
					"FzNr": "101 1212 12 -12",
					"Type": "BT1 500",
					"Object": "Steckdose Seitenwand im Wagen",
					"State": "kein Strom",
					"Team": "L",
					"Date": "21.05.2019 11.15",
					"longtext": "Loreawdm ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
					"system": "Bremsen pneum. & Mg-Bremse -> Klotzbremsanlage -> Bremssohle",
					"symptom": "abgenutzt"
				}, {
					"Status": "offen",
					"Priority": "hoch",
					"FzNr": "101 1212 12 -12",
					"Type": "BT1 500",
					"Object": "Steckdose Seitenwand im Wagen",
					"State": "kein Strom",
					"Team": "L",
					"Date": "21.05.2019 11.15",
					"longtext": "Lorem iffffpsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
					"system": "Bremsen pneum. & Mg-Bremse -> Klotzbremsanlage -> Bremssohle",
					"symptom": "abgenutzt"
				}, {
					"Status": "offen",
					"Priority": "hoch",
					"FzNr": "101 1212 12 -12",
					"Type": "BT1 500",
					"Object": "Steckdose Seitenwand im Wagen",
					"State": "kein Strom",
					"Team": "L",
					"Date": "21.05.2019 11.15",
					"longtext": "Lorem ipsum dolor sitggggggg amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
					"system": "Bremsen pneum. & Mg-Bremse -> Klotzbremsanlage -> Bremssohle",
					"symptom": "abgenutzt"
				}, {
					"Status": "offen",
					"Priority": "hoch",
					"FzNr": "101 1212 12 12",
					"Type": "BT1 500",
					"Object": "Steckdose Seitenwand im Wagen",
					"State": "kein Strom",
					"Team": "L",
					"Date": "21.05.2019 11.15",
					"longtext": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
					"system": "Bremsen pneum. & Mg-Bremse -> Klotzbremsanlage -> Bremssohle",
					"symptom": "abgenutzt"
				}, {
					"Status": "offen",
					"Priority": "hoch",
					"FzNr": "101 1212 12 12",
					"Type": "BT1 500",
					"Object": "Steckdose Seitenwand im Wagen",
					"State": "kein Strom",
					"Team": "L",
					"Date": "21.05.2019 11.15",
					"longtext": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
					"system": "Bremsen pneum. & Mg-Bremse -> Klotzbremsanlage -> Bremssohle",
					"symptom": "abgenutzt"
				}]
			};
			window.setTimeout(function () {

				this._bindUserListTable(oData.results);
			}.bind(this), 2000);

		},

		/**
		 * Creates a table template for the notification list
		 * @method _tableFactoryFunction
		 * @public
		 * @param {string} sId - the created control ID
		 * @param {sap.ui.model.Context} oContext - the context object which allows accessing data from the list entry
		 * @return {sap.m.ColumnListItem} oColumnListItem - Row of the Table
		 */
		_tableFactoryFunction: function (sId, oContext) {

			var oCellStatus = new Text({
				text: {
					path: "Status"
				}
			});

			var oCellPriority = new Text({
				text: {
					path: "Priority"
				}
			});
			var oCellFzNr = new Text({
				text: {
					path: "FzNr"
				}
			});
			var oCellType = new Text({
				text: {
					path: "Type"
				}
			});
			var oCellObject = new Text({
				tooltip: "{Object}",
				text: {
					path: "Object"
				},
				wrapping: false

			});
			var oCellState = new Text({
				text: {
					path: "State"
				}
			});
			var oCellTeam = new Text({
				text: {
					path: "Team"
				}
			});
			var oCellDate = new Text({
				text: {
					path: "Date"
				}
			});

			var oColumnListItem = new ColumnListItem({
				type: "Active",
				press: this.onItemPress.bind(this),
				cells: [oCellStatus, oCellPriority, oCellFzNr, oCellType, oCellObject, oCellState, oCellTeam, oCellDate],
				tooltip: ""
			});
			return oColumnListItem;
		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Event handler when a notificationlistitem is pressed
		 * Updates the bottom part of the view with detail information
		 * @method onItemPress
		 * @example
		 * The second row gets pressed, then the bounded Object will be saved.
		 * Later if the object is needed, you can find it there:
		 * this._oView.getModel("LocalNotificationListModel").setProperty("/selection/rowSelected");
		 * @public
		 * @param {sap.ui.base.Event} oEvent the table selectionChange event			 
		 */
		onItemPress: function (oEvent) {

			var selectedRow = oEvent.getParameters().listItem.getBindingContext().getObject();

			var sPathObject = "/selection/row";
			var sPathStatus = "/selection/rowSelected";
			var LocalNotificationListModel = this._oView.getModel("LocalNotificationListModel");
			LocalNotificationListModel.setProperty(sPathObject, selectedRow);
			LocalNotificationListModel.setProperty(sPathStatus, true);

		},

		/**
		 * handler on route match
		 * @method _onObjectMatched
		 * @public
		 * @param {sap.ui.base.Event} oPatternMatchedEvent - the pattern match event in route 'object'
		 */
		_onObjectMatched: function (oPatternMatchedEvent) {

			if (!this._oTable) {
				this._oTable = this.byId("notificationTable");
				var iOriginalBusyDelay = this._oTable.getBusyIndicatorDelay();

				this._oTable.attachEventOnce("updateFinished", function () {
					this._oTable.setBusy(false);
					//	this._oView.getModel().setProperty("/tableBusyDelay", iOriginalBusyDelay);
				}.bind(this));

				this._oTable.attachEvent("updateFinished", function () {

				}.bind(this));

				this._fnReadNotifiactionDataSet();

			}

		},

		/**
		 * Event handler when a row of the notification table is pressed.
		 * @method onSelectNotification
		 * @public
		 * @param {sap.ui.base.Event} oEvent		 
		 */
		onSelectNotification: function (oEvent) {
			this.getOwnerComponent().getRouter().navTo("NotificationDetails");

		},

		/**
		 * Event handler when something is typed in the search field.
		 * @method onHandleSearch
		 * @public
		 * @example <caption>Example usage of onHandleSearch.</caption>
		 * onHandleSearch("s");
		 * returns all locations which contains the letter s
		 * @param {sap.ui.base.Event} oEvent		 
		 */
		onHandleSearch: function (evt) {
			// create model filter
			var filters = [];
			var query = evt.getParameters().newValue;
			if (query && query.length > 0) {
				var filter = new sap.ui.model.Filter("location", sap.ui.model.FilterOperator.Contains, query);
				filters.push(filter);
			}
			// update list binding
			var table = this.getView().byId("idProductsTable");
			var binding = table.getBinding("items");
			binding.filter(filters);
		}
	});
});