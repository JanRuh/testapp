sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"sbb/repairAndClean/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	/**
	 * @class
	 * @extends sap.ui.core.UIComponent
	 * @name sbb.repairAndClean.Component
	 */

	return UIComponent.extend("sbb.repairAndClean.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 * @memberOf sbb.repairAndClean.Component
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		}
	});
});