sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		/** 
		 * Create Device JSONModel
		 * @public
		 * @module  sbb.repairAndClean.model.models
		 * @returns {sap.ui.model.json.JSONModel} oModel - Device JSONModel
		 */
		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		}

	};
});