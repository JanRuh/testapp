const jsdoc2md = require('jsdoc-to-markdown');
const fs = require('fs');
const path = require('path');

module.exports = function (grunt) {
	"use strict";

	grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-build");
	var aFilePaths = grunt.file.expand({
		filter: "isFile"
	}, ["./webapp/*.js", "./webapp/controller/*.js", "./webapp/model/*.js"]);

	if (grunt.file.exists("./README.hbs")) {
		grunt.option("sReadmeFileContent", grunt.file.read("./README.hbs").toString());
	} else {
		grunt.log.writeln("No README.template file found in directory 'webapp'.");
	}

	this.allFiles = []
	aFilePaths.forEach(sFilePath => this.allFiles.push({
		src: [sFilePath],
		dest: './docs/' +
			sFilePath.slice(sFilePath.lastIndexOf("/") + 1, sFilePath.length - 3) + '.md'
	}));
	grunt.option("aJSDocFiles", this.allFiles)

	// Creating the .MD files for all *.js Files.
	grunt.registerTask('jsdoc2md', 'API documentation generator', function () {
		const options = {}
		const done = this.async()
		const promises = grunt.option("aJSDocFiles").map(function (file) {
			const outputPath = file.dest
			grunt.file.mkdir(path.dirname(outputPath))
			options.files = file.src
			options.template = grunt.option("sReadmeFileContent")

			grunt.log.oklns('writing ' + outputPath)
			return jsdoc2md.render(options)
				.then(function (output) {
					fs.writeFileSync(outputPath, output)
				})
				.catch(grunt.fail.fatal)
		})
		Promise.all(promises).then()
	});

	// Reading values from Manifest 
	if (grunt.file.exists("./webapp/manifest.json")) {
		//	var sManifest = grunt.file.readJSON("./webapp/manifest.json"),
		//		sDsn = sManifest["sap.ui5"].models[""].dataSource;
		//	grunt.option("sDsnUri", sManifest["sap.app"].dataSources[sDsn.toString()].uri);
		grunt.option("sAppName", grunt.file.readJSON("./webapp/manifest.json")["sap.app"].id);
		grunt.option("sAppVersion", grunt.file.readJSON("./webapp/manifest.json")["sap.app"].applicationVersion.version);
	} else {
		grunt.log.writeln("No manifest.json file found in directory 'webapp'.");
	}

	// Reading template of README file to be included in JSDoc.
	if (grunt.file.exists("./README.template")) {
		grunt.option("sReadmeFileContentTemplate", grunt.file.read("./README.template").toString());
	} else {
		grunt.log.writeln("No README.template file found in directory 'webapp'.");
	}

	// Creating the README file to be included in JSDoc.
	grunt.registerTask("jsdoc-preprocess", "Generate 'README.MD'", function () {

		var sJSDocFiles =  grunt.option("aJSDocFiles").map(function (file) {
			return "[" + file.src[0].slice(file.src[0].lastIndexOf("/")+1) + "](" + file.dest + ")"
		}).join(" <br> ")

		grunt.option("sJSDocFiles", sJSDocFiles)


//		- [RepairAndCleanReport.js](docs / RepairAndCleanReport.controller.md)

		console.log(grunt.option("sJSDocFiles"))
		grunt.file.write("./README.MD", grunt.option("sReadmeFileContentTemplate")
			.replace("$APPNAME", grunt.option("sAppName"))
			.replace("$VERSION", grunt.option("sAppVersion"))
			.replace("$ODATAURI", "serviceNotExists")
			.replace("$JSDOCFILES", grunt.option("sJSDocFiles")));
		grunt.log.writeln("README.MD created.");
	});

	grunt.registerTask("default", [
		"clean",
		"lint",
		"build",
		"jsdoc-preprocess",
		//					"jsdoc",
		"jsdoc2md"
	]);
}

//			//var pdf = require('html-pdf');
//			var path = require('path');
//			//var phantomjs = require('phantomjs-prebuilt');
//			var fs = require('fs');
//			var html5pdf = require('html5-to-pdf');
//			
//			module.exports = function (grunt) {
//				"use strict";
//			
//				grunt.initConfig({
//					"wkhtmltopdf": {
//						dev: {
//							src: __dirname + 'docs/docs-sbb.repairAndClean-1.0.2/index.html',
//							dest: './xxx.pdf'
//						},
//						prod: {
//							src: __dirname + 'docs/docs-sbb.repairAndClean-1.0.2/index.html',
//							dest: './xxx.pdf'
//						}
//					}
//				});
//				grunt.loadNpmTasks('grunt-wkhtmltopdf');
//				// Fetching application version and OData path.
//				if (grunt.file.exists("./webapp/manifest.json")) {
//					//	var sManifest = grunt.file.readJSON("./webapp/manifest.json"),
//					//		sDsn = sManifest["sap.ui5"].models[""].dataSource;
//					//	grunt.option("sDsnUri", sManifest["sap.app"].dataSources[sDsn.toString()].uri);
//					grunt.option("sAppName", grunt.file.readJSON("./webapp/manifest.json")["sap.app"].id);
//					grunt.option("sAppVersion", grunt.file.readJSON("./webapp/manifest.json")["sap.app"].applicationVersion.version);
//				} else {
//					grunt.log.writeln("No manifest.json file found in directory 'webapp'.");
//				}
//			
//				// Reading template of README file to be included in JSDoc.
//				if (grunt.file.exists("./README.template")) {
//					grunt.option("sReadmeFileContent", grunt.file.read("./README.template").toString());
//				} else {
//					grunt.log.writeln("No README.template file found in directory 'webapp'.");
//				}
//			
//				// Load and configure the original SAP build strap.
//				grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-build");
//				grunt.config.merge({
//					compatVersion: "edge"
//				});
//			
//				// Load and configure the JSDoc task.
//				grunt.loadNpmTasks("grunt-jsdoc");
//				grunt.config("jsdoc", {
//					src: ["./webapp/*.js", "./webapp/controller/*.js", "./webapp/model/*.js", "README.MD"],
//					options: {
//						destination: "./docs/docs-" + grunt.option("sAppName") + "-" + grunt.option("sAppVersion"),
//						private: true
//					}
//				});
//			
//				// Creating the README file to be included in JSDoc.
//				grunt.registerTask("jsdoc-preprocess", "Generate 'README.MD'", function () {
//					grunt.file.write("./README.MD", grunt.option("sReadmeFileContent")
//						.replace("$APPNAME", grunt.option("sAppName"))
//						.replace("$VERSION", grunt.option("sAppVersion"))
//						.replace("$ODATAURI", "serviceNotExists"));
//					grunt.log.writeln("README.MD created.");
//				});
//			
//				//		"phantomjs-prebuilt": "^2.1.16",
//				//	"path": "^0.12.7",
//				//	"html-pdf": "^2.1.0"
//				//	grunt.registerTask('html_pdf', 'Grunt wrapper for the Node package html-pdf', function () {
//				//		var options = this.options({
//				//			separator: "\n"
//				//		});
//				//		grunt.log.writeln("XXXXXXXXXXXXX");
//				//		var done = this.async(),
//				//			async = grunt.util.async;
//				//		var files = 'docs/docs-sbb.repairAndClean-1.0.2/index.html';
//				//		var afiles = [{
//				//			src: ['docs/docs-sbb.repairAndClean-1.0.2/index.html'],
//				//			dest: 'docs/xxff.pdf'
//				//		}];
//				//		var count = 7; //this.files.length;
//				//		grunt.log.ok("count value is:  " + count);
//				//		//	var src = grunt.file.read(files);
//				//		//	grunt.log.writeln("XXXXXXXXvvvvv" + src.toString());
//				//		//
//				//		//	pdf.create(src, options).toFile("docs/xxx.pdf", function (err, res) {
//				//		//		grunt.log.writeln("Successfully created" + err);
//				//		//		grunt.log.writeln("XXXXXXXXXXXXX");
//				//		//		grunt.log.writeln("Successfully created" + res);
//				//		//		grunt.log.writeln("XXXXXXXXXXXXX");
//				//		//	});
//				//
//				//		async.forEach(afiles, function (f) {
//				//			grunt.log.writeln("CCCCCCCCCC");
//				//			grunt.log.writeln(f.src[0]);
//				//
//				//			var src = f.src.filter(function (filepath) {
//				//					grunt.log.warn(filepath);
//				//					if (!grunt.file.exists(filepath)) {
//				//						grunt.log.warn("Source file \"" + filepath + "\" not found.");
//				//						return false;
//				//					} else {
//				//						path = filepath;
//				//						return true;
//				//					}
//				//				})
//				//				.map(function (filepath) {
//				//					grunt.log.warn("filepatth" + filepath);
//				//					return grunt.file.read(filepath);
//				//				})
//				//				.join(grunt.util.normalizelf(options.separator));
//				//
//				//			grunt.log.warn("dest" + f.dest);
//				//			var optionsv = {
//				//				phantomPath: phantomjs.path,
//				//				filename: "hallo.pdf",
//				//				format: 'A4',
//				//				orientation: 'portrait',
//				//				type: "pdf",
//				//				timeout: 30000
//				//			};
//				//			grunt.log.warn("path phantom: " + __dirname + phantomjs.path);
//				//
//				//			pdf.create(src, optionsv).toFile('/docs/', function (err, res) {
//				//				grunt.log.warn("err" + err);
//				//				grunt.log.ok("Successfully created " + res);
//				//			});
//				//		});
//				//	});
//			
//				grunt.registerTask('html_pdf', 'Grunt wrapper for the Node package html-pdf', function () {
//					grunt.log.writeln("CCCCCCCCCC");
//			
//					fs.createReadStream("docs/docs-sbb.repairAndClean-1.0.2/index.html")
//						.pipe(html5pdf())
//						.pipe(fs.createWriteStream("/path/to/document.pdf"));
//			
//					// --- OR ---
//			
//					html5pdf().from("docs/docs-sbb.repairAndClean-1.0.2/index.html").to("/path/to/document.pdf", function () {
//						console.log("Done");
//					});
//			
//				});
//			
//				// Defining the tasks in order to perform.
//				grunt.registerTask("default", [
//					"clean",
//					"lint",
//					"build",
//					"jsdoc-preprocess",
//					"jsdoc",
//					"html_pdf"
//				]);
//			};

//module.exports = function (grunt) {
//	'use strict';
//	grunt.initConfig({
//		jsdoc2md: {
//			oneOutputFile: {
//				src: "./webapp/controller/*.js",
//				dest: './api/documentation.md'
//			},
//			separateOutputFilePerInput: {
//				files: [{
//					src: './webapp/controller/BaseController.js',
//					dest: './api/Base.md'
//				}]
//			},
//			withOptions: {
//				options: {
//					'no-gfm': true
//				},
//				src: './webapp/Component.js',
//				dest: './api/with-index.md'
//			}
//		}
//	});
//
//	grunt.loadNpmTasks('grunt-jsdoc-to-markdown');
//
//	grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-build");
//	grunt.registerTask("default", [
//		"clean",
//		"lint",
//		"build",
//		"jsdoc2md"
//	]);
//};

//			// module.exports = function (grunt) {
//			// 	"use strict";
//			// 	grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-build");
//			// 	grunt.registerTask("default", [
//			// 		"clean",
//			// 		"lint",
//			// 		"build"
//			// 	]);
//			// };
//			
//			module.exports = function (grunt) {
//				"use strict";
//			
//			
//				grunt.initConfig({
//					html_pdf: {
//						dist: {
//							options: {
//								format: 'A4',
//								orientation: 'portrait',
//								quality: '75',
//							},
//							files: {
//								'./docs/mypdf.pdf': ['./docs/docs-sbb.repairAndClean-1.0.2/index.html', ],
//							},
//						}
//					},
//				});
//				// Fetching application id.
//				//	if (grunt.file.exists("./webapp/config.json")) {
//				//		grunt.option("sAppName", grunt.file.readJSON("./webapp/config.json").ApplicationID);
//				//	} else {
//				//		grunt.log.writeln("No config.json file found in directory 'webapp'.");
//				//	}
//			
//				// Fetching application version and OData path.
//				if (grunt.file.exists("./webapp/manifest.json")) {
//					//	var sManifest = grunt.file.readJSON("./webapp/manifest.json"),
//					//		sDsn = sManifest["sap.ui5"].models[""].dataSource;
//					//	grunt.option("sDsnUri", sManifest["sap.app"].dataSources[sDsn.toString()].uri);
//					grunt.option("sAppName", grunt.file.readJSON("./webapp/manifest.json")["sap.app"].id);
//					grunt.option("sAppVersion", grunt.file.readJSON("./webapp/manifest.json")["sap.app"].applicationVersion.version);
//				} else {
//					grunt.log.writeln("No manifest.json file found in directory 'webapp'.");
//				}
//			
//				// Reading template of README file to be included in JSDoc.
//				if (grunt.file.exists("./README.template")) {
//					grunt.option("sReadmeFileContent", grunt.file.read("./README.template").toString());
//				} else {
//					grunt.log.writeln("No README.template file found in directory 'webapp'.");
//				}
//			
//				// Load and configure the original SAP build strap.
//				grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-build");
//				grunt.config.merge({
//					compatVersion: "edge"
//				});
//			
//				// Load and configure the JSDoc task.
//				grunt.loadNpmTasks("grunt-jsdoc");
//				grunt.config("jsdoc", {
//					src: ["./webapp/*.js", "./webapp/controller/*.js", "./webapp/model/*.js", "README.MD"],
//					options: {
//						destination: "./docs/docs-" + grunt.option("sAppName") + "-" + grunt.option("sAppVersion"),
//						private: true
//					}
//				});
//			
//				// Creating the README file to be included in JSDoc.
//				grunt.registerTask("jsdoc-preprocess", "Generate 'README.MD'", function () {
//					grunt.file.write("./README.MD", grunt.option("sReadmeFileContent")
//						.replace("$APPNAME", grunt.option("sAppName"))
//						.replace("$VERSION", grunt.option("sAppVersion"))
//						.replace("$ODATAURI", "serviceNotExists"));
//					grunt.log.writeln("README.MD created.");
//				});
//			
//				// Defining the tasks in order to perform.
//				grunt.registerTask("default", [
//					"clean",
//					"lint",
//					"build",
//					"grunt-html-pdf-2"
//				]);
//				
//				grunt.loadNpmTasks('grunt-html-pdf-2');
//			};